<?php
if (empty($_POST)) {
    header('location:idenx.php');
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <title></title>
    </head>
    <body>
        <section class="container">
            <div class="col-md-offset-2 col-md-8">
                <div class="panel panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Send Message</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered table-striped  table-hover">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Tel</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $qty = explode("\n", $_POST['contactos']);
                                foreach ($qty as $te) {
                                    $explode = str_getcsv($te);
                                    ?>
                                    <tr>
                                        <td><?= $explode[0] ?></td>
                                        <td><?= $explode[1] ?></td>
                                        <td><button type="button" class="btn btn-success" onclick="sendWp('<?= trim($explode[0]) ?>', '<?= trim($explode[1]) ?>')">Send Whatsapp</button></td>
                                    </tr>    

                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </section>
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Message WP</h4>
                    </div>
                    <div class="modal-body">
                        <iframe id="content"></iframe>
                    </div> 
                </div>
            </div>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script>
            function sendWp(name,tel) {
                window.open('https://wa.me/57'+tel+"?text=<?= urlencode("\n".$_POST['mensaje']) ?>");
            }
        </script>
        
    </body>
</html>


